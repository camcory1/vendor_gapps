$(call inherit-product, vendor/gapps/common-blobs.mk)

# Include package overlays
DEVICE_PACKAGE_OVERLAYS += \
    vendor/gapps/overlay/

# framework
PRODUCT_PACKAGES += \
    com.google.android.dialer.support \
    com.google.android.maps \
    com.google.android.media.effects

# app
PRODUCT_PACKAGES += \
    FaceLock \
    GoogleContactsSyncAdapter \
    GoogleExtShared \
    GoogleTTS \
    PrebuiltBugle \
    talkback

# priv-app
PRODUCT_PACKAGES += \
    AndroidPlatformServices \
    ConfigUpdater \
    ConnMetrics \
    GoogleBackupTransport \
    GoogleContacts \
    GoogleDialer \
    GoogleExtServices \
    GoogleFeedback \
    GoogleOneTimeInitializer \
    GooglePackageInstaller \
    GooglePartnerSetup \
    GoogleServicesFramework \
    Phonesky \
    PrebuiltGmsCore \
    SetupWizard \
    StorageManagerGoogle \
    Velvet
